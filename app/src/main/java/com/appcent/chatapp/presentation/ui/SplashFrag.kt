package com.appcent.chatapp.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import com.appcent.chatapp.databinding.FragSplashBinding
import kotlinx.coroutines.delay

import kotlinx.coroutines.launch

class SplashFrag : Fragment(){

    private lateinit var binding: FragSplashBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragSplashBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            delay(2000)
            val request = NavDeepLinkRequest.Builder
                .fromUri("android-app://com.appcent.features.item/list".toUri())
                .build()
            findNavController().navigate(request)
        }
    }
}