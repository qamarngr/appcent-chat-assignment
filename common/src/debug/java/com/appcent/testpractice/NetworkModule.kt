package com.appcent.chatapp

import com.appcent.data.cache.ChatDao
import com.appcent.data.entity.ChatResponse
import com.appcent.data.module.RepositoryModule
import com.appcent.data.network.ChatService
import com.appcent.data.network.ktor.ChatItemService
import com.appcent.data.repository.ItemRepositoryImpl
import com.appcent.data.repository.ChatRepositoryRepositoryImpl
import com.appcent.domain.model.ChatResponseDTO
import com.appcent.domain.model.status.Result
import com.appcent.domain.repository.ItemRepository
import com.appcent.domain.repository.ChatRepositoryRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton

@Module
@TestInstallIn(
	components = [SingletonComponent::class],
	replaces = [RepositoryModule::class]
)
object FakeModule {

	@Provides
	fun provideChatService(): ChatService = object : ChatService {
		override suspend fun fetchChats(page: Int, perPage: Int): Result<ChatResponse> {
			val photoList = listOf(
				ChatResponseDTO(
					id = "1",
					description = "first",
				)
			)
			val response = ChatResponse(
				photoList, 1
			)

			return Result.Success(response, 200)
		}
	}

	@Singleton
	@Provides
	fun provideChatRepository(
		unsplashService: ChatService
	): ChatRepositoryRepository {
		return ChatRepositoryRepositoryImpl(unsplashService)
	}

	@Singleton
	@Provides
	fun provideItemRepository(
		chatItemService: ChatItemService,
		chatDao: ChatDao
	): ItemRepository = ItemRepositoryImpl(chatItemService, chatDao)

}