enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

pluginManagement {
	repositories {
		gradlePluginPortal()
		google()
		mavenCentral()
	}
}
rootProject.name = "AppcentChat"

val modules = arrayOf(
	":app", ":domain", ":data", ":presentation", ":features:chat", ":common"
)
include(*modules)
