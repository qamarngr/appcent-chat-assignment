package com.appcent.domain.repository

import androidx.paging.PagingData
import com.appcent.domain.model.ChatResponseDTO
import kotlinx.coroutines.flow.Flow
import com.appcent.domain.model.status.Result

interface ChatRepositoryRepository {
    fun fetchChats(page: Int): Flow<Result<List<ChatResponseDTO>>>

    //fun insertChat(name: String): Flow<Result<List<ChatResponseDTO>>>

    fun getSearchResult(query: String): Flow<PagingData<ChatResponseDTO>>
}