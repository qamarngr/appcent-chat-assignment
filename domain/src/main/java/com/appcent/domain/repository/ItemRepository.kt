package com.appcent.domain.repository

import com.appcent.domain.model.ChatItem
import kotlinx.coroutines.flow.Flow

interface ItemRepository {
    fun getItems(): Flow<List<ChatItem>>
    suspend fun insertItem(chatItem: ChatItem): Long
}