package com.appcent.domain.model

data class ChatItem(
    val id: Int = 0,
    val name: String
)