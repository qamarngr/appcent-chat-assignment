package com.appcent.domain.model

data class ChatResponseDTO(
    val id: String,
    val description: String?,

) {

    override fun equals(other: Any?): Boolean {
        return id == (other as ChatResponseDTO).id
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}