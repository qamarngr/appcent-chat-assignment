package com.appcent.domain.interactor

import com.appcent.domain.model.ChatItem
import com.appcent.domain.repository.ItemRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InsertChatUseCase @Inject constructor(
    private val itemRepository: ItemRepository
) {
    suspend fun insert(chatItem: ChatItem) : Long {
        return itemRepository.insertItem(chatItem)
    }
}