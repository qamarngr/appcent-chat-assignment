package com.appcent.data.network.ktor

import com.appcent.data.entity.ChatResponse
import com.appcent.data.network.ChatService

import com.appcent.domain.model.status.Result
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import javax.inject.Inject
import javax.inject.Named

class ChatServiceImpl @Inject constructor(
    @Named("unsplash") private val httpClient: HttpClient
) : ChatService {

    override suspend fun fetchChats(
        page: Int,
        perPage: Int
    ): Result<ChatResponse> {
        return try {
            val response = httpClient.get("search/photos") {
                parameter("page", page)
                parameter("per_page", perPage)
            }.body<HttpResponse>()

            if (response.status.isSuccess()) {
                Result.Success(response.body(), response.status.value)
            } else {
                Result.ApiError(response.status.description, response.status.value)
            }
        } catch (e: Exception) {
            Result.NetworkError(e)
        }
    }
}