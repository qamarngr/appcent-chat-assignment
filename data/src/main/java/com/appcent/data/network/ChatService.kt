package com.appcent.data.network

import com.appcent.data.entity.ChatResponse
import com.appcent.domain.model.status.Result

interface ChatService {

    suspend fun fetchChats(page: Int = 1, perPage: Int = 20): Result<ChatResponse>
}