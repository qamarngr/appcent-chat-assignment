package com.appcent.data.network.ktor

import com.appcent.data.entity.ChatEntity
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import com.appcent.domain.model.status.Result
import javax.inject.Inject
import javax.inject.Named

class ChatItemService @Inject constructor(
    @Named("chats") private val httpClient: HttpClient
) {

    suspend fun getItemList(): Result<List<ChatEntity>> {
        return try {
            val response = httpClient.get {  }

            if (response.status.isSuccess()) {
                Result.Success(response.body(), response.status.value)
            } else {
                Result.ApiError(response.status.description, response.status.value)
            }
        } catch (e: Exception) {
            Result.NetworkError(e)
        }
    }
}