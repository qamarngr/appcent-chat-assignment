package com.appcent.data.network.retrofit

import com.appcent.data.entity.ChatResponse
import com.appcent.data.network.ChatService
import com.appcent.domain.model.status.Result
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface RetrofitUnsplashService : ChatService {

    @Headers("Accept-Version: v1", "Authorization: Client-ID $CLIENT_ID")
    @GET("chat/list")
    override suspend fun fetchChats(
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): Result<ChatResponse>

    companion object {
        private const val CLIENT_ID = "ti90oMOJyxTN-gKrvE39bi6LM2tbMAdOvey4QMKES0k"
    }
}