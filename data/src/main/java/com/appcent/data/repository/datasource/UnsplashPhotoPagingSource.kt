package com.appcent.data.repository.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.appcent.data.network.ChatService
import com.appcent.domain.model.ChatResponseDTO
import com.appcent.domain.model.status.Result

class UnsplashPhotoPagingSource constructor(
    private val service: ChatService,
    private val query: String
) : PagingSource<Int, ChatResponseDTO>() {
    override fun getRefreshKey(state: PagingState<Int, ChatResponseDTO>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ChatResponseDTO> {
        val position = params.key?: 1
        val response = service.fetchChats(position, params.loadSize)

        return if (response is Result.Success) {
            LoadResult.Page(
                data = response.data.results,
                prevKey = if (position == 1) null else position - 1,
                nextKey = if (position == response.data.totalPages) null else position + 1
            )
        } else {
            LoadResult.Error(Exception())
        }
    }
}