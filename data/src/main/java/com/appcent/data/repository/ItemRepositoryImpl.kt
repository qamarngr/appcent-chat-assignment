package com.appcent.data.repository

import com.appcent.data.cache.ChatDao
import com.appcent.data.entity.ChatEntity
import com.appcent.data.mapper.ItemConverter.mapFromDomainModel
import com.appcent.data.mapper.ItemConverter.mapToDomainModel
import com.appcent.data.network.ktor.ChatItemService
import com.appcent.domain.model.ChatItem
import com.appcent.domain.repository.ItemRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import com.appcent.domain.model.status.Result
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class ItemRepositoryImpl @Inject constructor(
    private val chatItemService: ChatItemService,
    private val chatDao: ChatDao
) : ItemRepository {

    override fun getItems(): Flow<List<ChatItem>> = flow {
        when (val response = chatItemService.getItemList()) {
            is Result.Success -> {
                chatDao.insertItem(response.data)
                emit(response.data.map { it.mapToDomainModel() })
            }
            is Result.ApiError, is Result.NetworkError -> {
                emitAll(chatDao.getItemList().map { it.mapToDomainModel() })
            }
            else -> Unit
        }
    }

    override suspend fun insertItem(chatItem: ChatItem): Long {

            val existingItems = chatDao.getChatWithName(chatItem.name)
        return if (existingItems.isNotEmpty()) {
            -1
        } else {
            when (val inserted = chatDao.insertItem(chatItem.mapFromDomainModel())) {
                -1L -> -1
                else -> inserted
            }
        }


    }

    private fun List<ChatEntity>.mapToDomainModel() =
        map { it.mapToDomainModel() }
}