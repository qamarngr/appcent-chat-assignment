package com.appcent.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.appcent.data.mapper.ResponseConverter
import com.appcent.data.network.ChatService
import com.appcent.data.repository.datasource.UnsplashPhotoPagingSource
import com.appcent.domain.model.ChatResponseDTO
import com.appcent.domain.model.status.Result
import com.appcent.domain.repository.ChatRepositoryRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChatRepositoryRepositoryImpl @Inject constructor(
    private val unsplashService: ChatService
) : ChatRepositoryRepository {
    override fun fetchChats(page: Int) = flow {
        unsplashService.fetchChats(page, 10).run {
            emit(ResponseConverter.responseToPhotoList(this))
        }
    }

    override fun getSearchResult(query: String) =
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize = 100,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { UnsplashPhotoPagingSource(unsplashService, query) }
        ).flow
}