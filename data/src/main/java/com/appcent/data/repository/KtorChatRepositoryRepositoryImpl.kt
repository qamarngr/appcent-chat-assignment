package com.appcent.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.appcent.data.mapper.ResponseConverter
import com.appcent.data.network.ChatService
import com.appcent.data.repository.datasource.UnsplashPhotoPagingSource
import com.appcent.domain.model.ChatResponseDTO
import com.appcent.domain.repository.ChatRepositoryRepository
import com.appcent.domain.model.status.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class KtorChatRepositoryRepositoryImpl @Inject constructor(
    private val unsplashService: ChatService
) : ChatRepositoryRepository {

    override fun fetchChats(
        page: Int
    ): Flow<Result<List<ChatResponseDTO>>> = flow {
        val response = unsplashService.fetchChats(page)
        emit(ResponseConverter.responseToPhotoList(response))
    }

    override fun getSearchResult(query: String) =
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize = 100,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { UnsplashPhotoPagingSource(unsplashService, query) }
        ).flow
}