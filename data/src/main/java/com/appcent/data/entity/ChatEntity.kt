package com.appcent.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "chats")
data class ChatEntity(
    val name: String,
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
)