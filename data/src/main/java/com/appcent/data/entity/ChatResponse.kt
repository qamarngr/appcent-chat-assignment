package com.appcent.data.entity

import com.appcent.domain.model.ChatResponseDTO
import com.google.gson.annotations.SerializedName

data class ChatResponse(
    val results: List<ChatResponseDTO>,
    @SerializedName("total_pages")
    val totalPages: Int
)