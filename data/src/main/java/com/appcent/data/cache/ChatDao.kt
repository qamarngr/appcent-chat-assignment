package com.appcent.data.cache

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.appcent.data.entity.ChatEntity

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

@Dao
interface ChatDao {

    @Query("SELECT * FROM chats")
    fun getItemList(): Flow<List<ChatEntity>>

    @Insert
    suspend fun insertItem(item: ChatEntity): Long

    @Insert
    suspend fun insertItem(item: List<ChatEntity>)
    @Insert
    suspend fun insertItem(vararg item: ChatEntity): LongArray

    @Query("SELECT * FROM chats WHERE name = :query")
    fun getChatWithName(query: String): List<ChatEntity>


}