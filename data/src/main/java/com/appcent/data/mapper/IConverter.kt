package com.appcent.data.mapper

interface IConverter<From, To> {
    fun From.mapToDomainModel(): To
    fun To.mapFromDomainModel(): From
}