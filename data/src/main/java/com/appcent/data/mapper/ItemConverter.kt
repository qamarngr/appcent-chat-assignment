package com.appcent.data.mapper

import com.appcent.data.entity.ChatEntity
import com.appcent.domain.model.ChatItem

object ItemConverter : IConverter<ChatEntity, ChatItem> {
    override fun ChatEntity.mapToDomainModel(): ChatItem =
        ChatItem(id, name)

    override fun ChatItem.mapFromDomainModel(): ChatEntity =
        ChatEntity(name)
}