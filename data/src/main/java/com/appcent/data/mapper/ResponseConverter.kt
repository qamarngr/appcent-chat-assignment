package com.appcent.data.mapper

import com.appcent.data.entity.ChatResponse
import com.appcent.domain.model.ChatResponseDTO
import com.appcent.domain.model.status.Result

object ResponseConverter {

    fun responseToPhotoList(response: Result<ChatResponse>): Result<List<ChatResponseDTO>> {
        return when(response) {
            is Result.Success -> Result.Success(response.data.results, response.code)
            is Result.ApiError -> Result.ApiError(response.message, response.code)
            is Result.NetworkError -> Result.NetworkError(response.throwable)
            is Result.NullResult -> Result.NullResult()
            is Result.Loading -> Result.Loading()
        }
    }
}