package com.appcent.features.item

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.appcent.domain.model.ChatItem
import com.appcent.features.item.databinding.ItemChatBinding

class ItemAdapter : ListAdapter<ChatItem, ItemAdapter.ItemViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder =
        ItemViewHolder(
            ItemChatBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)

        holder.binding.apply {
            name.text = item.name
        }
    }

    class ItemViewHolder(val binding: ItemChatBinding) : RecyclerView.ViewHolder(binding.root)
}

val COMPARATOR = object : DiffUtil.ItemCallback<ChatItem>() {
    override fun areItemsTheSame(oldChatItem: ChatItem, newChatItem: ChatItem): Boolean =
        oldChatItem == newChatItem

    override fun areContentsTheSame(oldChatItem: ChatItem, newChatItem: ChatItem): Boolean =
        oldChatItem == newChatItem

}