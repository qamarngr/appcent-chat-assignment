package com.appcent.features.item

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.appcent.domain.interactor.GetChatListUseCase
import com.appcent.domain.interactor.InsertChatUseCase
import com.appcent.domain.model.ChatItem
import com.appcent.domain.repository.ItemRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ItemViewModel @Inject constructor(
    getItem: GetChatListUseCase,
    private val insertItem: InsertChatUseCase,
) : ViewModel() {

    val itemList = getItem()
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), emptyList())

    var insertedItem = MutableStateFlow<ChatItem?>(null)
    fun onButtonClick(chatItem: ChatItem) {
        viewModelScope.launch(Dispatchers.IO) {
            insertedItem.value = ChatItem(insertItem.insert(chatItem).toInt(), chatItem.name)
        }
    }
}