package com.appcent.features.item

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.appcent.features.item.databinding.FragChatListBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ItemFragment : Fragment(R.layout.frag_chat_list) {

    private val itemViewModel: ItemViewModel by viewModels()

    private lateinit var binding: FragChatListBinding
    private lateinit var itemAdapter: ItemAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragChatListBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        itemAdapter = ItemAdapter()

        binding.rvItem.adapter = itemAdapter

        binding.fabAdd.setOnClickListener {
            findNavController().navigate(R.id.newChatFragment)
        }

        lifecycleScope.launch {
            itemViewModel.itemList.flowWithLifecycle(lifecycle).collect {
                if (it.isNotEmpty()) {
                    itemAdapter.submitList(it)
                    binding.tvEmptyMessage.visibility = View.GONE
                } else {
                    binding.tvEmptyMessage.visibility = View.VISIBLE
                }
            }
        }
    }
}