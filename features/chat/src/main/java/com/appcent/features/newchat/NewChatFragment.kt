package com.appcent.features.newchat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.appcent.domain.model.ChatItem
import com.appcent.features.item.ItemViewModel
import com.appcent.features.item.R
import com.appcent.features.item.databinding.FragNewChatBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class NewChatFragment : Fragment(R.layout.frag_chat_list) {

    private val itemViewModel: ItemViewModel by viewModels()

    private lateinit var binding: FragNewChatBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragNewChatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnSave.setOnClickListener {
            itemViewModel.onButtonClick(ChatItem(0,binding.editTextText.text.toString()))
        }

        lifecycleScope.launch {
            itemViewModel.insertedItem.collect {
                when (it?.id) {
                    -1 -> {
                        findNavController().popBackStack()
                        withContext(Dispatchers.Main) {
                            Toast.makeText(context,
                               R.string.msg_chat_exists, Toast.LENGTH_LONG).show()
                        }
                    }

                    null->{}
                    else -> {
                        findNavController().popBackStack()
                    }
                }
            }
        }
    }
}